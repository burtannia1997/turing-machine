import TuringMachine
import qualified Data.Map as Map (fromList)

hello :: TM
hello = TM
    { tmInit = 0
    , tmFinals = [5]
    , tmTrans = [ Transition 0 1 Blank (S 'h') R
                , Transition 1 2 Blank (S 'e') R
                , Transition 2 3 Blank (S 'l') R
                , Transition 3 4 Blank (S 'l') R
                , Transition 4 5 Blank (S 'o') R
                ]
    }

eightOne :: TM
eightOne = TM
    { tmInit = 0
    , tmFinals = [8]
    , tmTrans = [ Transition 0 1 Blank (S '1') R
                , Transition 1 2 Blank (S '1') R
                , Transition 2 3 Blank (S '1') R
                , Transition 3 4 Blank (S '1') R
                , Transition 4 5 Blank (S '1') R
                , Transition 5 6 Blank (S '1') R
                , Transition 6 7 Blank (S '1') R
                , Transition 7 8 Blank (S '1') R
                ]
    }

unaryAdd :: TM
unaryAdd = TM
    { tmInit = 0
    , tmFinals = [1]
    , tmTrans = [ Transition 0 1 Blank Blank R
                , Transition 0 2 (S '1') (S '1') R

                , Transition 2 2 (S '1') (S '1') R
                , Transition 2 3 Blank (S '1') R

                , Transition 3 3 (S '1') (S '1') R
                , Transition 3 4 Blank Blank L
                
                , Transition 4 1 (S '1') Blank L
                ]
    }

addTape :: Tape
addTape = Map.fromList
    [ (0, (S '1'))
    , (1, (S '1'))
    , (2, Blank)
    , (3, (S '1'))
    , (4, (S '1'))
    , (5, (S '1'))
    , (6, (S '1'))
    ]