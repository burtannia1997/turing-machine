{-# LANGUAGE RecordWildCards #-}

module TuringMachine where

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (fromMaybe, listToMaybe)
import Data.Foldable (asum)

data TM = TM
    { tmInit   :: State
    , tmFinals :: [State]
    , tmTrans  :: [Transition]
    }

type State = Int

data Transition = Transition
    { tStart :: State
    , tNext  :: State
    , tRead  :: Symbol
    , tWrite :: Symbol
    , tMove  :: Move
    }

data Symbol = Blank | S Char
    deriving Eq

instance Show Symbol where
    show Blank = show '_'
    show (S c) = show c

-- Left, Right, None
data Move = L | R | N

type Tape = Map Int Symbol

runTM :: TM -> Tape -> Int -> Maybe Tape
runTM TM{..} tape pos = runTM' tmInit tape pos
    where
        runTM' state tape pos
            | state `elem` tmFinals = return tape
            | otherwise = asum $ map move trans
            where
                symb
                    = fromMaybe Blank (Map.lookup pos tape)
                trans
                    = filter (\t -> tStart t == state && tRead t == symb) tmTrans
                move Transition{..}
                    = runTM' tNext tape' pos'
                    where
                        tape' = Map.insert pos tWrite tape
                        pos'  = case tMove of
                            L -> pos - 1
                            R -> pos + 1
                            N -> pos

run :: TM -> Tape -> Int -> IO ()
run tm tape pos = case runTM tm tape pos of
    Nothing -> print "Nothing"
    Just t  -> print $ Map.elems t

emptyTape :: Tape
emptyTape = Map.fromList []